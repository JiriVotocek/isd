// Declare your global variables here
volatile unsigned char SPI_message2RF[32];
volatile unsigned char SPI_message2CPU[32];
volatile unsigned char temperature[2];
volatile unsigned char analog[2];
volatile unsigned char SPItries;
volatile unsigned char command2RF;
volatile unsigned char I2Ctransfer;

volatile unsigned int tempRoom[MAXSAMPLECNT]; 
volatile unsigned int tempGrad[MAXSAMPLECNT];
volatile unsigned char calcAverageRoomTindex;
volatile unsigned int averageRoomTindex;
volatile unsigned int tRoom;
volatile unsigned int tempObj;
volatile unsigned int tempA;
volatile unsigned int tempGradAct;
volatile unsigned int tempGradindex;
volatile unsigned char batV;
volatile unsigned char batTimeout;
volatile unsigned char clockI2Con;
volatile unsigned int timerTemp;
volatile unsigned int timerGrad;
volatile unsigned int timerHush;
volatile unsigned int timerTest;
volatile unsigned char SRD;                     //Status register of detector
volatile unsigned char SRC;                     //Status register of command
volatile unsigned char SRD2;                    //Status register 2 of detector
volatile unsigned char SRC2;                    //Status register 2 of command
volatile unsigned char SRL;                     //Status register of loop
volatile unsigned char SRL2;                    //Status register 2 of loop 
volatile unsigned char eepromAddr;

volatile unsigned char sampleCNT;
volatile unsigned char gCondLimit;
volatile unsigned char gCondCNT;
volatile unsigned char tCondLimit;
volatile unsigned char tCondCNT;
volatile unsigned char gTempMIN;
volatile unsigned int tFireCondCNT;
volatile unsigned int tHushCondCNT;
volatile unsigned int tTestCondCNT;
volatile unsigned char buttonPressed;
volatile unsigned char pyroFlags;


#asm
.eseg
.org 0x00
    .db "ISD V39.006"
.org 0x0D
    .db 13
    .db 0
    .db 0


.org 0x20
     //One period now is 4s
    .db 15         //Sample CNT => 15 corresponds with 15*4s = 60s
    .db 15         //Gradient condition -> (temp[sample CNT]-temp[0])*100/sampleCNT
                   //15 corespods with 0,15 �C/sample - 0,0375 �C/s    
    .db 10         //Gradient condition for KTY -> (temp[sample CNT]-temp[0])*100/sampleCNT
                   //10 corespods with 0,10 �C/sample - 0,025 �C/s    
    .db 5          //Gradient condition positive CNT
    .db 5          //Temperature condition + TRoom
    .db 5          //Temperature condition + TRoom for KTY
    .db 5          //Temperature condition positive CNT  - 20s
    .db 40         //Min Object Temperature to start evaluate Gradient condition.
    .db 30         //Temperature confition positive CNT to indicate fire alarm - two minute
    .db 120        //Temperature confition positive CNT to indicate fire alarm - 10 minutes
#endasm

#asm
.eseg
.org 0x00
    .db "ISD V39.006"
.org 0x0D
    .db 13
    .db 0
    .db 0


.org 0x20
     //One period now is 4s
    .db 15         //Sample CNT => 15 corresponds with 15*4s = 60s
    .db 15         //Gradient condition -> (temp[sample CNT]-temp[0])*100/sampleCNT
                   //15 corespods with 0,15 �C/sample - 0,0025 �C/s    
    .db 10         //Gradient condition for KTY -> (temp[sample CNT]-temp[0])*100/sampleCNT
                   //10 corespods with 0,10 �C/sample - 0,00167 �C/s    
    .db 5          //Gradient condition positive CNT
    .db 5          //Temperature condition + TRoom
    .db 5          //Temperature condition + TRoom for KTY
    .db 5          //Temperature condition positive CNT  - 20s
    .db 40         //Min Object Temperature to start evaluate Temperature condition. 
    .db 30         //Min Object Temperature to start evaluate Temperature condition for KTY .
    .db 30         //Temperature confition positive CNT to indicate fire alarm - two minute
    .db 120        //Hush timeout - 10 minutes
    .db 15	   //Test timeout - 1 minute
#endasm

//Table calculated for AD conversion 9-bit Vt/3.3V * 512
//Starts at -55�C 
//End at 175�C
//Step is 1�C
__flash unsigned int lookupKTY[231] =
{
93,93,94,95,96,97,98,99,100,101,            
102,103,104,105,106,107,108,109,110,111,
112,113,114,115,116,117,118,119,120,122,
123,124,125,126,127,128,129,131,132,133,
134,135,136,137,139,140,141,142,143,145,
146,147,148,150,151,152,153,155,156,157,
159,160,161,162,164,165,166,168,169,170,
172,173,175,176,177,179,180,181,183,184,
186,187,189,190,191,193,194,196,197,199,
200,202,203,205,206,208,209,211,212,214,
215,217,219,220,222,223,225,226,228,230,
231,233,234,236,238,239,241,243,244,246,
248,249,251,253,254,256,258,260,261,263,
265,267,268,270,272,274,275,277,279,281,
283,284,286,288,290,292,294,295,297,299,
301,303,305,307,309,310,312,314,316,318,
320,322,324,326,328,330,332,334,336,338,
340,342,344,346,348,350,352,354,356,358,
360,362,364,366,368,370,373,375,377,379,
381,383,385,387,390,392,394,396,398,400,
403,405,407,409,412,414,416,418,420,423,
425,427,430,432,434,436,439,441,443,446,
448,450,453,455,457,460,462,464,467,469,
471
};
/*
__flash unsigned char lookupKTY[231] =
{
//Table calculated for AD conversion 8-bit
//Starts at -55�C 
//End at 175�C
//Step is 1�C
46,46,47,47,48,48,49,49,50,50,
51,51,52,52,53,53,54,54,55,55,
56,56,57,57,58,58,59,59,60,61,
61,62,62,63,63,64,64,65,66,66,
67,67,68,68,69,70,70,71,71,72,
73,73,74,75,75,76,76,77,78,78,
79,80,80,81,82,82,83,84,84,85,
86,86,87,88,88,89,90,90,91,92,
93,93,94,95,95,96,97,98,98,99,
100,101,101,102,103,104,104,105,106,107,
107,108,109,110,111,111,112,113,114,115,
115,116,117,118,119,119,120,121,122,123,
124,124,125,126,127,128,129,130,130,131,
132,133,134,135,136,137,137,138,139,140,
141,142,143,144,145,146,147,147,148,149,
150,151,152,153,154,155,156,157,158,159,
160,161,162,163,164,165,166,167,168,169,
170,171,172,173,174,175,176,177,178,179,
180,181,182,183,184,185,186,187,188,189,
190,191,192,193,195,196,197,198,199,200,
201,202,203,204,206,207,208,209,210,211,
212,213,215,216,217,218,219,220,221,223,
224,225,226,227,228,230,231,232,233,234,
235
};
*/