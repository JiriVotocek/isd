/*******************************************************
This program was created by the CodeWizardAVR V3.28 
Automatic Program Generator
� Copyright 1998-2016 Pavel Haiduc, HP InfoTech s.r.l.
http://www.hpinfotech.com

Project : ISD
Version : 
Date    : 25.02.2021
Author  : 
Company : Sicom AS
Comments: 
FW to control stove detector.


Chip type               : ATtiny44
AVR Core Clock frequency: 4,000000 MHz
Memory model            : Tiny
External RAM size       : 0
Data Stack size         : 64
*******************************************************/
///EEARH = 0  zm�na oproti Atmel 44A
#include <io.h>
#include <delay.h>
#include <math.h>
#include <isd.h>
#include <isd_var.h>

// External Interrupt 0 service routine
interrupt [EXT_INT0] void ext_int0_isr(void)
{
    unsigned char i;
    #asm("cli");   
    //Global interupt disable is not enough here
    //Need to disable INT0 mask
    GIMSK=(0<<INT0);
    GIFR=(1<<INTF0);
    // Place your code here  
    buttonPressed = 0; 
    delay_ms(100);
    for (i = 0;i<10;++i){
        if (!PUSHBUTTON) ++buttonPressed;
        else break;
        delay_ms(10);
    }
}

// Watchdog timeout interrupt service routine
interrupt [WDT] void wdt_timeout_isr(void)
{
    // Place your code here
    #pragma optsize-
    #asm("wdr")
    WDTCSR|=(1<<WDCE) | (1<<WDE);
    WDTCSR =(1<<WDIF) | (0<<WDIE) | (1<<WDP3) | (0<<WDCE) | (1<<WDE) | (0<<WDP2) | (0<<WDP1) | (0<<WDP0);
    #ifdef _OPTIMIZE_SIZE_
    #pragma optsize+
    #endif       
}


void main(void)
{
unsigned char tmp;
unsigned int i;
    // Declare your local variables here
    // I/O Port init
    CPUinit();
    
    clockI2Con = 0; 
    
    //Clear of Status Register of detector
    // ALARM + ALARM MEMORY + ERROR + INITIALISATION + COMMUNICATION - DIAG. MODE
    // NOTE + BATT + PreAlarm 
    SRD = 0;       
    //Clear of Status Register 2 of detector
    // TEST + Diagnostic + RF program uppload + Controll of RF module
    // Settling of sensor + Reset of detector + KTY sensor connected + 9V battery connected 
    SRD2 = 0; 
    //Clear of Status Register of command
    // ALARM + ALARM MEMORY + ERROR + INITIALISATION + COMMUNICATION - DIAG. MODE
    // NOTE + BATT + PreAlarm 
    SRC = 0;   
    //Clear of Status Register 2 of command
    // TEST + Diagnostic + RF program uppload + Controll of RF module
    // Settling of sensor + Reset of detector + KTY sensor connected + 9V battery connected 
    SRC2 = 0;                              
    //Clear of Status Register of Loop
    // ALARM + ALARM MEMORY + ERROR + INITIALISATION + COMMUNICATION - DIAG. MODE
    // NOTE + BATT + PreAlarm 
    SRL = 0;   
    //Clear of Status Register 2 of Loop
    // TEST + Diagnostic + RF program uppload + Controll of RF module
    // Settling of sensor + Reset of detector + KTY sensor connected + 9V battery connected 
    SRL2 = 0;   
       
    tempGradindex = 0;
    //Test battery after power is switched on
    // ADC init
    ADC_Init();
    ALARM_LED = 1;
    delay_ms(10);  
    tmp = read_adc(KTYIN);
    tmp = 0;
    ALARM_LED = 0;
    SENSORKTYPOWERPININ; 
    SENSORKTYPOWERHZ;
    while(!PUSHBUTTON);
    if (tmp == 0xFF){
       ADC_Init();    
       BitSet(SRD2,POWERIN);               
       FAULT_LED = 1;
       delay_ms(10);  
       tmp = read_adc(POWERIN);
       FAULT_LED = 0;               
    }  
    ALLSENSORMAINPOWERON;
    i = read_adcVBAT(KTYIN);
    analog[0] = i;
    
    if ((i > KTYASSEMBLED)&&(i<MAXKTYASSEMBLED)){
        BitSet(SRD2,KTYSENS);       
    }  
    SET_MLXSCLIN;   
    ADCSRA = 0;   
    sampleCNT = EEPROM_read(EESAMPLESCNT);
    if (BitTest(SRD2,KTYSENS)==0) gCondLimit = EEPROM_read(EEGRADCOND);
    else gCondLimit = EEPROM_read(EEGRADCONDKTY);
    gCondCNT = EEPROM_read(EEGRADCNT);
    if (BitTest(SRD2,KTYSENS)==0) tCondLimit = EEPROM_read(EETEMPCOND);
    else tCondLimit = EEPROM_read(EETEMPCONDKTY);
    tCondCNT = EEPROM_read(EETEMPCNT); 
    if (BitTest(SRD2,KTYSENS)==0) gTempMIN = EEPROM_read(EEMINTEMPOBJ);
    else gTempMIN = EEPROM_read(EEMINTEMPOBJKTY);
    tFireCondCNT = EEPROM_read(EEFIRETEMPCNT); 
    tHushCondCNT = EEPROM_read(EEHUSHCNT); 
    tTestCondCNT = EEPROM_read(EETESTCNT);  
    SPItries = 0;
    tempGradindex = 0;
    batTimeout = 0;
    calcAverageRoomTindex = 0;
    for(tmp = 0; tmp < sampleCNT;++tmp){
        tempRoom[tmp] = 0xFF; 
    }                                             
    averageRoomTindex = 0;
    
    tRoom = 0xFF;
    command2RF = SPI2RF_NOTHING;                  
// Globally enable interrupts
#asm("sei")

i = 0;
eepromAddr = 0x30;
SET_MOSIOUT; 
SET_MISOIN;
SET_SCKOUT;  
    
while (1)
      {    
          // Test Battery
          batV = ReadBatteryVoltage(batV); 
          
          // Get data from sensor
          ReadTempSensor();
          
          // Temperature level and gradient testing 
          checkTempLimits();          
          
          // Different Timer handling 
          timerHandler();
                                                     
          // Set Status Register of Detector
          SRDsetting();
                          
          buttonFunction();
          
          Indication();       
          
          if (buttonPressed == 0){
              CreateRFmessage();                                              
              //SPI message transfer to RFmodule
              i = SPI_Transfer2RF(4); 
              ++SPItries;
              if ((i == 0x3F)||(SPItries >= MAXSPITRIESCNT)){
                //If diagnostic message should be sent
                if (BitTest(SRD2,DFLG)==1){
                   delay_ms(200); 
                   if (BitTest(SRC2,DFLG)==0) DiagnosticMessage(63);
                   else DiagnosticMessage(10);
                }         
                //Set SRC, because SPI has been successful
                SRCsetting();  
                //Set SRL, according to received SPI command 
                SRLsetting();
                if (SPItries<MAXSPITRIESCNT) SPItries = 0;
                else SPItries = MAXSPITRIESCNT;
                command2RF = SPI2RF_NOTHING;
              }           
          }                              
          SET_MOSIIN;
          SET_MISOIN;
          SET_SCKIN;
          // Initialisation goes in 1s period
          // Standard cycle is 4s period
          if ((buttonPressed == 0)&&(BitTest(SRD,IFLG)==0)){
              // Watchdog Timer initialization
              // Watchdog Timer Prescaler: OSC/512k  - 4s
              // Watchdog timeout action: Interrupt          
              #pragma optsize-
              #asm("wdr")
              WDTCSR|=(1<<WDCE) | (1<<WDE);
              WDTCSR=(1<<WDIF) | (1<<WDIE) | (1<<WDP3) | (0<<WDCE) | (1<<WDE) | (0<<WDP2) | (0<<WDP1) | (0<<WDP0);
              #ifdef _OPTIMIZE_SIZE_
              #pragma optsize+
              #endif
          }                     
          else{
              // Watchdog Timer initialization
              // Watchdog Timer Prescaler: OSC/512k  - 1s
              // Watchdog timeout action: Interrupt
              #pragma optsize-
              #asm("wdr")
              WDTCSR|=(1<<WDCE) | (1<<WDE);
              WDTCSR=(1<<WDIF) | (1<<WDIE) | (0<<WDP3) | (0<<WDCE) | (1<<WDE) | (1<<WDP2) | (1<<WDP1) | (0<<WDP0);
              #ifdef _OPTIMIZE_SIZE_
              #pragma optsize+
              #endif
          } 
          //Enable Sleep
          MCUCR |= (1<<SE) | (1<<SM1);
          #asm("sleep");            
      }
}


//Init of CPU after power device on.
void CPUinit(void)
{
   // Crystal Oscillator division factor: 1
    #pragma optsize-
    CLKPR=(1<<CLKPCE);
    CLKPR=(0<<CLKPCE) | (0<<CLKPS3) | (0<<CLKPS2) | (0<<CLKPS1) | (0<<CLKPS0);
    #ifdef _OPTIMIZE_SIZE_
    #pragma optsize+
    #endif

    // Input/Output Ports initialization
    // Port A initialization
    // Function: Bit7=Out Bit6=Out Bit5=In Bit4=Out Bit3=Out Bit2=In Bit1=Out Bit0=In 
    DDRA=(1<<DDA7) | (1<<DDA6) | (0<<DDA5) | (1<<DDA4) | (1<<DDA3) | (0<<DDA2) | (1<<DDA1) | (0<<DDA0);
    // State: Bit7=0 Bit6=0 Bit5=T Bit4=0 Bit3=1 Bit2=T Bit1=0 Bit0=T 
    PORTA=(0<<PORTA7) | (0<<PORTA6) | (0<<PORTA5) | (0<<PORTA4) | (1<<PORTA3) | (0<<PORTA2) | (0<<PORTA1) | (0<<PORTA0);

    // Port B initialization
    // Function: Bit3=In Bit2=In Bit1=Out Bit0=Out 
    DDRB=(0<<DDB3) | (0<<DDB2) | (1<<DDB1) | (1<<DDB0);
    // State: Bit3=T Bit2=T Bit1=0 Bit0=0 
    PORTB=(0<<PORTB3) | (0<<PORTB2) | (0<<PORTB1) | (0<<PORTB0);

    // Timer/Counter 0 initialization
    // Clock source: System Clock
    // Clock value: Timer 0 Stopped
    // Mode: Normal top=0xFF
    // OC0A output: Disconnected
    // OC0B output: Disconnected
    TCCR0A=(0<<COM0A1) | (0<<COM0A0) | (0<<COM0B1) | (0<<COM0B0) | (0<<WGM01) | (0<<WGM00);
    TCCR0B=(0<<WGM02) | (0<<CS02) | (0<<CS01) | (0<<CS00);
    TCNT0=0x00;
    OCR0A=0x00;
    OCR0B=0x00;

    // Timer/Counter 1 initialization
    // Clock source: System Clock
    // Clock value: Timer1 Stopped
    // Mode: Normal top=0xFFFF
    // OC1A output: Disconnected
    // OC1B output: Disconnected
    // Noise Canceler: Off
    // Input Capture on Falling Edge
    // Timer1 Overflow Interrupt: Off
    // Input Capture Interrupt: Off
    // Compare A Match Interrupt: Off
    // Compare B Match Interrupt: Off
    TCCR1A=(0<<COM1A1) | (0<<COM1A0) | (0<<COM1B1) | (0<<COM1B0) | (0<<WGM11) | (0<<WGM10);
    TCCR1B=(0<<ICNC1) | (0<<ICES1) | (0<<WGM13) | (0<<WGM12) | (0<<CS12) | (0<<CS11) | (0<<CS10);
    TCNT1H=0x00;
    TCNT1L=0x00;
    ICR1H=0x00;
    ICR1L=0x00;
    OCR1AH=0x00;
    OCR1AL=0x00;
    OCR1BH=0x00;
    OCR1BL=0x00;

    // Timer/Counter 0 Interrupt(s) initialization
    TIMSK0=(0<<OCIE0B) | (0<<OCIE0A) | (0<<TOIE0);

    // Timer/Counter 1 Interrupt(s) initialization
    TIMSK1=(0<<ICIE1) | (0<<OCIE1B) | (0<<OCIE1A) | (0<<TOIE1);

    // External Interrupt(s) initialization
    // INT0: On
    // INT0 Mode: Low level
    // Interrupt on any change on pins PCINT0-7: Off
    // Interrupt on any change on pins PCINT8-11: Off
    MCUCR=(0<<ISC01) | (0<<ISC00);
    GIMSK=(1<<INT0) | (0<<PCIE1) | (0<<PCIE0);
    GIFR=(1<<INTF0) | (0<<PCIF1) | (0<<PCIF0);

    // USI initialization
    // Mode: Disabled
    // Clock source: Register & Counter=no clk.
    // USI Counter Overflow Interrupt: Off
    USICR=(0<<USISIE) | (0<<USIOIE) | (0<<USIWM1) | (0<<USIWM0) | (0<<USICS1) | (0<<USICS0) | (0<<USICLK) | (0<<USITC);

    // Analog Comparator initialization
    // Analog Comparator: Off
    // The Analog Comparator's positive input is
    // connected to the AIN0 pin
    // The Analog Comparator's negative input is
    // connected to the AIN1 pin
    ACSR=(1<<ACD) | (0<<ACBG) | (0<<ACO) | (0<<ACI) | (0<<ACIE) | (0<<ACIC) | (0<<ACIS1) | (0<<ACIS0);
    // Digital input buffer on AIN0: On
    // Digital input buffer on AIN1: On
    DIDR0=(0<<ADC1D) | (0<<ADC2D);

    // ADC initialization
    // ADC disabled
    ADCSRA=(0<<ADEN) | (0<<ADSC) | (0<<ADATE) | (0<<ADIF) | (0<<ADIE) | (0<<ADPS2) | (0<<ADPS1) | (0<<ADPS0);
}

#define SPICLKPERIOD 5 
void SPI_Byte2RF(unsigned char data){
    USIDR = data;     
    SS = 0;
    delay_us(40);
    //Clear overflow flag in USI register
    USISR = (1<<USIOIF);                 
    SPIINTERFACE_ON;  
    //delay_us(SPICLKPERIOD);
    //Wait until SPI byte transmition is finished 
    //TODO: solve some SPI CLK frequency
    while(!(USISR&(1<<USIOIF))){           
    //    delay_us(10);
        delay_us(SPICLKPERIOD);
        SPIINTERFACE_ON; // Toggles SCK pin 
    }                 
    delay_us(40);
    SS = 1;
    delay_us(250);
    delay_us(250);  
}

unsigned char SPI_Transfer2RF(unsigned char SPIDLEN){
unsigned char dataAUX,CRCM,CRCS,i;
    SET_SCKOUT;
    SCK = 0;          
    delay_ms(1);
    SET_MOSIOUT;
    MOSI = 0; 
    SET_MISOIN;
    MISO = 1;
    //SET_SCKOUT;
    //SCK = 0;          
    SPI_Byte2RF(SPI_CHECK);         //First sends SPI_CHECK
    SPI_Byte2RF(SPI_CHECK);         //First sends SPI_CHECK
    dataAUX = USIDR;
    if  (dataAUX == 0x80){
        SPI_Byte2RF(SPI_CMD);
        SPI_Byte2RF(PTYPE+SPIDLEN);
        CRCM = SPI_CMD^(PTYPE+SPIDLEN);
        CRCS = (PTYPE+SPIDLEN);               
        for(i = 0; i<SPIDLEN;++i){
            SPI_Byte2RF(SPI_message2RF[i]);
            SPI_message2CPU[i] = USIDR; 
            CRCM = CRCM^SPI_message2RF[i]; 
            CRCS = CRCS^SPI_message2CPU[i];      
        }
        CRCM = CRCM^0x5F; 
        CRCS = CRCS^0x5F; 
        SPI_Byte2RF(CRCM); 
        dataAUX = USIDR;          
        SPI_Byte2RF(SPI_CHECK);         
        if  ((USIDR == 0x3F)&&(dataAUX == CRCS)){
            dataAUX = USIDR;
        }                  
        else{
            dataAUX = 0xFF;
        }
    }
    else{ 
        dataAUX = 0xFE;
    }
    USICR = 0x00;
    SET_MOSIIN; 
    SET_SCKIN;
    return(dataAUX);               
}

unsigned char ReadTempSensor(){
    unsigned char tmp;
    if (buttonPressed > 0) return(0);  
    // Get data from KTY
      if (BitTest(SRD2,KTYSENS) == 1){
          ALLSENSORMAINPOWERON;
          tempObj = 0xFF00;
          tempA = 0xFF00;
          delay_ms(5);
          tmp = ReadKTY();
          if (tmp < 0xF0){
              tempObj = 0x00FF&temperature[1];
              tempObj = tempObj << 8; 
              tempObj += temperature[0];
              tempA = tempObj;
          }
          //When there is Hush State the tempObj is equal to tempA
          if (BitTest(SRD,HFLG)) tempObj = tempA;
          
          //Store to the buffer and calculate temp gradient
          storeTempAndGradCalc(tempObj); 
          
          //Store ambient temperature and calculate room temperature
          AverageRoomTempCalc(tempA);        
          ALLSENSORMAINPOWEROFF;         
      }
      else{ 
          // Get data from sensor  
          I2Ctransfer = 0;
          tempObj = 0xFF00;
          tempA = 0xFF00;
          pyroFlags = 0; 
          I2C_Pyro2WakeUP();  
                  
          pyroFlags = I2C_Byte2PyroFlags(0); 
                    
          //Temperature of the object of view 
          tmp = I2C_Byte2Pyro(I2C_ADR,COMMAND_Tobj); 
          //I2C transfer was OK?
          if (tmp<0xF0){   
              tempObj = 0x00FF&temperature[1];
              tempObj = tempObj << 8; 
              tempObj += temperature[0]; 
              //Devide by 50 gives output in K  
              //15 095 / 50 = 301K = 28,75�C
              tempObj = tempObj/50;
              //Convert to �C
              if (tempObj >= 273){         
                //Positive temperature + 0�C 
                //301K - 273 = 28,75�C
                tempObj = tempObj - 273; 
                //Reset the most significant bit 
                tempObj = tempObj&0x7FFF;
              }
              else{
                //Negative temperature
                //244K - 273 = -29,15�C     
                //273 - 244K  = 29,15�C
                tempObj = 273-tempObj;
                //Set the most significant bit - calculate   
                //65536 - 29 = 65 507 + 1= 65 508
                //65536 - 29 = 65 507 + 1= 65 508 
                //65536 = 0�C,65535 = - 1�C,65534 = - 2�C                 
                tempObj = ~tempObj+1;
              }   
          }
          else I2Ctransfer |= 0x01;  
                  
          //Tempareture of the sensor itself
          tmp = I2C_Byte2Pyro(I2C_ADR,COMMAND_Ta);
          //I2C Transfer was OK?  
          if (tmp<0xF0){            
              tempA = temperature[1];
              tempA = tempA << 8;
              tempA += temperature[0];
              //Devide by 50 to get output in K
              tempA = tempA/50;
              //Convert to �C
              tempA = tempA - 273;   
          }
          else I2Ctransfer |= 0x02;     
          
          //When there is Hush State the tempObj is equal to tempA
          if (BitTest(SRD,HFLG)) tempObj = tempA;
                   
          //Store to the buffer and calculate temp gradient
          storeTempAndGradCalc(tempObj); 
          
          //Store ambient temperature and calculate room temperature
          AverageRoomTempCalc(tempA);
          /*        
          //Temperature in room is defined:
          // if Temp of the sensor is in the range <15�C,28�C)
          if ((tempA >= 16) && (tempA < 30)){ 
            // Temp of object is the same as temp of sensor  +/- 2�C               
            if ((tempA <= (tempObj+2))&&(tempA > (tempObj-2))) tRoom = tempA;
            // Initialize room temperature buffer
            for(tmp = 0;tmp<sampleCNT-1;++tmp){
                tempRoom[tmp] = tRoom;
            }            
          }                 
          */        
          tmp = I2C_Pyro2Sleep(0);
          if (tmp>=0xF0)I2Ctransfer |= 0x03;                 
      }
      return(1);                    
}


#define I2CCLKPERIOD    5 //I2CCLKPERIOD = I2Cperiod / 2  - > in us
#define PYROWAKEUPTIME  35 //in ms
// I2Crate now corresponds with 
unsigned char I2CWrite(unsigned char data){
unsigned char i,y,ack;
    //Send data
    delay_us(I2CCLKPERIOD);
    delay_us(I2CCLKPERIOD);
    for(i = 0; i<8; ++i){
       //Send the hignest bit first 
       y = data;
       y = (y>>(7-i)); 
       MLXSDA = 0x01&y;
       delay_us(1);
       MLXSCL = 1;
       delay_us(I2CCLKPERIOD-1);
       delay_us(I2CCLKPERIOD);        
       MLXSCL = 0;
       if (i == 7) SET_MLXSDAIN;              
       delay_us(I2CCLKPERIOD);
       delay_us(I2CCLKPERIOD);        
    }
    /*for(i = 0; i<8; ++i){
       delay_us(1);
       y = data;
       y = (y>>(7-i));
       MLXSDA = 0x01&y;
       delay_us(1);
       MLXSCL = 1;
       delay_us(I2CCLKPERIOD);
       delay_us(I2CCLKPERIOD);        
       MLXSCL = 0;
       if (i == 7) SET_MLXSDAIN;              
       delay_us(I2CCLKPERIOD);
       delay_us(I2CCLKPERIOD);        
    }*/
    //Get ACK (0) or NACK (1)
    ack = I2CgetAck();
    SET_MLXSDAOUT;
    #asm("nop");   
    return(ack);                
}

unsigned char I2CRead(unsigned char ack){
unsigned char i,data;
    SET_MLXSDAIN;
    MLXSDA = 1;    
    data = 0;       
    //Read data    
    for(i = 0; i<8; ++i){
       #asm("nop");
       #asm("nop");  
       //SCL = 1
       MLXSCL = 1;
       //Wait for half period until SDA is read
       delay_us(I2CCLKPERIOD);                 
       //Shift previous data to left
       data = data << 1;            
       //Read SDA pin value
       if (MLXSDAin) data = data+0x01;
       else data = data+0x00;
       //Wait next half period 
       delay_us(I2CCLKPERIOD);  
       //Set SCL = 0
       MLXSCL = 0;
       delay_us(I2CCLKPERIOD);
       delay_us(I2CCLKPERIOD);
    }    
    //Change SDA to Output
    SET_MLXSDAOUT;
    #asm("nop");
    //Set SDA ACK or NACK according to ack parameter.  
    if (ack == 1)MLXSDA = 1;
    else MLXSDA = 0;
    //Clock with SCL
    MLXSCL = 1;  
    delay_us(I2CCLKPERIOD);
    delay_us(I2CCLKPERIOD);
    MLXSCL = 0;
    delay_us(I2CCLKPERIOD);
    delay_us(I2CCLKPERIOD);
    MLXSDA = 0;
    return(data);                
}

void I2CStartCond(){
    //SET SCL as input, SDA as output
    delay_us(I2CCLKPERIOD);
    SET_MLXSDAOUT;
    MLXSDA = 1;
    #asm("nop");
    SET_MLXSCLIN;
    MLXSCL = 1;        
    #asm("nop"); 
    MLXSDA = 1;               
    //SCL = 1
    //SDA = 1              
    //Wait for one period
    delay_us(I2CCLKPERIOD);
    //Start condition: 
    //SCL = 1, SDA = 1 => 0  
    MLXSDA = 0;            
    //Wait for one period
    delay_us(I2CCLKPERIOD); 
    //Change SCL to Output and set SCL = 0
    SET_MLXSCLOUT;   
    MLXSCL = 0;  
    #asm("nop");
}

//Fucntion returns ACK or NACK read on SDA pin
//SDA pin is set as input
unsigned char I2CgetAck(){
unsigned char ack;
    //ACK check: SDA == 1 - OK, SDA == 0 - FAULT
    #asm("nop");
    delay_us(1);
    //SET SCL = 1
    MLXSCL = 1;  
    //Wait half period  
    delay_us(I2CCLKPERIOD-1);
    //Read SDA 
    if (MLXSDAin == 0) ack = 0;        
    else ack = 1;       
    //Wait second half period  
    delay_us(I2CCLKPERIOD);
    MLXSCL = 0;  
    return(ack);
}

void I2CStopCond(){
    //Stop condition
    //SCL = 1, SDA = 0 => 1  
    SET_MLXSDAOUT;   
    MLXSDA = 0;
    #asm("nop");
    #asm("nop");  
    //Set SCL = 1              
    MLXSCL = 1;  
    //Wait for half period              
    delay_us(I2CCLKPERIOD);
    //Set SDA = 1
    MLXSDA = 1;  
    //Wait for next half period
    delay_us(I2CCLKPERIOD);
}

//Function reads Pyro sensor values
//Returns 0xFF when noack 
//Returns 0 when ack and transmittion was successful
unsigned char I2C_Byte2Pyro(unsigned char sAddr,unsigned char data){
unsigned char SA;
unsigned char ack;
unsigned char dataCRC[5];
unsigned char length;
unsigned char pec;
    SET_MLXSCLOUT;
    MLXSCL = 0;        
    delay_ms(2);
    
    SA = sAddr;
    SA = SA<<1; 
    I2CStopCond();     
    //Generate Start Condition                
    I2CStartCond();
    //Send Address 
    dataCRC[0] = SA; 
    ack = I2CWrite(SA);
    if (ack == 1) return(0xff);
    //Send Command    
    dataCRC[1] = data; 
    ack = I2CWrite(data);                 // RAM address
    if (ack == 1) return(0xff);
    SA = SA|1; 
    //Generate start condition 
    I2CStartCond();
    //Send Address + 1
    dataCRC[2] = SA; 
    ack = I2CWrite(SA);             // MLX90614 address + read
	if (ack == 1) return(0xff);
    //Read the low byte with ACK
    temperature[0] = I2CRead(0);
    dataCRC[3] = temperature[0]; 
    //Read the high byte with ACK  
    temperature[1] = I2CRead(0);
    dataCRC[4] = temperature[1]; 
    //Read PEC with NACK
    pec = I2CRead(1);           
    //Generate Stop Condition
    I2CStopCond();  
    
    length = 5; 
    if (getCRC8(&dataCRC[0],length) == pec) return(0);
    else return(0xFF);
}

unsigned char getCRC8(unsigned char *data,unsigned char len){
unsigned char i,y,crcTest;
unsigned char GL;
    GL = 0b00000111; //CRC-8 polynom X8+X2+X1+1
    crcTest = 0;
    for(y = 0;y<len;++y){    
       crcTest ^= *(data+y);
       for(i = 0;i<8;++i){
           if BitTest(crcTest,7){
               crcTest = crcTest << 1; 
               crcTest = crcTest ^ GL;
           }
           else crcTest = crcTest << 1;
       }                                      
    }
    return(crcTest);
}
void I2C_Pyro2WakeUP(void){
    //SET SCL as input, SDA as output
    SET_MLXSCLIN;
    SET_MLXSDAOUT;
    MLXSCL = 1;        
    MLXSDA = 1;
    #asm("nop");
    #asm("nop"); 
    MLXSDA = 1;               
    //SCL = 1
    //SDA = 1              
    //Wait for one period
    delay_us(I2CCLKPERIOD); 
    //Wakeup condition: 
    //SCL = 1, SDA = 1 => 0 => 1  
    MLXSDA = 0;            
    delay_ms(PYROWAKEUPTIME);
    MLXSDA = 1;            
    delay_ms(250); 
}

unsigned char I2C_Pyro2Sleep(unsigned char sAddr){
unsigned char SA;
unsigned char ack;
unsigned char pec;
unsigned char dataCRC[2];
unsigned char length;
    SA = sAddr;
    SA = SA<<1;
    dataCRC[0] = SA;
    dataCRC[1] = COMMAND_SLEEP;
    length = 2; 
    pec = getCRC8(&dataCRC[0],length);
    
    //Generate Start Condition                
    I2CStartCond();
    //Send Address 
    ack = I2CWrite(SA);
    if (ack == 1) return(0xff);
    //Send Command
    ack = I2CWrite(COMMAND_SLEEP);                 // RAM address
    if (ack == 1) return(0xff);
    //Send PEC
    ack = I2CWrite(pec);                    // RAM address        
    if (ack == 1) return(0xff);
    //Generate Stop Condition
    I2CStopCond();     
    return(0);
}

unsigned char I2C_Byte2PyroFlags(unsigned char sAddr){
unsigned char I2Ccounter;
unsigned char SA;
unsigned char ack;
unsigned char dataL;
unsigned char dataH;
unsigned char dataCRC[5];
unsigned char length;
unsigned char pec;
    SA = sAddr;
    SA = SA<<1; 
    //Generate Start Condition                
    I2CStartCond();
    //Send Address      
    dataCRC[0] = SA;
    ack = I2CWrite(SA);
    if (ack == 1) return(0xff);
    //Send Command
    //ack = I2CWrite(COMMAND_FLAGS);                // RAM address
    dataCRC[1] = SA;
    ack = I2CWrite(0x04);                           // RAM address
    if (ack == 1) return(0xff);
    SA = SA|1; 
    //Generate start condition 
    I2CStartCond();
    //Send Address + 1
    dataCRC[2] = SA;
    ack = I2CWrite(SA);                             // MLX90614 address + read
	if (ack == 1) return(0xff);
    //Read the low byte with ACK
    dataL = I2CRead(0);         
    dataCRC[3] = dataL;
    //Read the high byte with ACK  
    dataH = I2CRead(0);
    dataCRC[4] = dataH;
    //Read PEC with NACK
    pec = I2CRead(1);           
    //Generate Stop Condition
    I2CStopCond();    

    length = 5; 
    if (getCRC8(&dataCRC[0],length) == pec) return(dataL);
    else return(0xFF); 
}

//Setting of SRD(status register of detector)
//SRD and SRD2 represents bits for different state of the detector
// SRD >>> IFLG - initialisation flag
// SRD >>> BFLG - low battery flag
// SRD >>> PFLG - prealarm flag
// SRD >>> FFLG - alarm flag

// SRD2 >>> TFLG - test flag
// SRD2 >>> DFLG - diagnostic flag
// SRD2 >>> PRGFLG - programing flag
// SRD2 >>> RMFLG - RM command flag
// SRD2 >>> SETLFLG - sensor settiling flag
// SRD2 >>> RSTFLG - Reset flag

//Not implemented yet
// SRD >>> EFLG - fault flag - Not implemented yet
// SRD >>> HFLG - Hush flag - Not implemented yet
// SRD >>> CFLG - comm. flag - Not implemented yet
// SRD >>> NFLG - note flag - Not implemented yet 
 
void SRDsetting(){
    if (tempGradindex<sampleCNT) BitSet(SRD,IFLG);
    else BitRST(SRD,IFLG);
    
    
    if (BitTest(SRD2,BAT9V) == 0){
        EEPROM_write(0x40,batV);
        //Is there 9V battery connected
        if (batV > MAXBAT3VCOND) BitSet(SRD2,BAT9V);
    }
    //Low battery is indicated 
    if (batV < MINBATCOND) BitSet(SRD,BFLG); 
    //Low battery has been detected - voltage is about 100mV higher 
    else if (batV > (MINBATCOND+6)) BitRST(SRD,BFLG);
    
    // OR fire prealarm condition
    // Temp of the sensor condition is OFF 
    //if (timerTemp > 0) BitSet(SRD,PFLG);
    //if (timerGrad > 0) BitSet(SRD,PFLG);
    //else BitRST(SRD,PFLG);                    
    
    // AND fire prealarm condition
    //When temperature of the object is too high for tCondCNT
    //When temperature gradient is too high for gCondCNT
    if ((timerTemp > tCondCNT)&&(timerGrad > gCondCNT)) BitSet(SRD,PFLG);   
    //When temperature of the object decrases under the temp limit
    else if (timerTemp == 0) BitRST(SRD,PFLG);  
    
    //****** OR fire alarm condition ********//
    //Fire alarm is indicated
    //When temperature of the sensor is too high for tCondCNT time 
    //if (timerTemp > tCondCNT) BitSet(SRD,FFLG);                    
    //When temperature gradient of the object is too high for gCondCNT time 
    //if (timerGrad > gCondCNT) BitSet(SRD,FFLG);
    //--------------------------------------//
    
    //****** AND fire alarm condition ********//
    //Fire alarm is indicated
    //When temperature of the sensor is too high for tFireCondCNT time
    //When gradient is too for two times tCondCNT time 
    //When Prealarm condition is fullfiled
    if ((timerTemp > tFireCondCNT)&&(BitTest(SRD,PFLG)==1)) BitSet(SRD,FFLG);                    
    if ((timerGrad >= 2*gCondCNT)&&(BitTest(SRD,PFLG)==1)) BitSet(SRD,FFLG);                    
    //--------------------------------------// 
    
    //****** Hush condition ********//
    //Prealarm was indicated and RST button has been pressed
    //Hush is indicated for 10min
    if (timerHush > tHushCondCNT) BitRST(SRD,HFLG); 
    if (BitTest(SRD,HFLG)) BitRST(SRD,PFLG);                   
    //--------------------------------------// 
    
    //****** Test condition ********//
    //RST button has been pressed
    //Test is indicated for 1min
    if (timerTest > tTestCondCNT) BitRST(SRD2,TFLG);                    
    //--------------------------------------// 
    
    //When room temperature is 0xFF settling of the sensor is activated
    if (tRoom == 0xFF) BitSet(SRD2,SETLFLG);
    else BitRST(SRD2,SETLFLG); 
    
    //RESET sollution
    if (BitTest(SRD2,RSTFLG) == 1){
       timerTemp = 0;
       timerGrad = 0;
       timerHush = 0;
       timerTest = 0;
       //Clear both status registers of detector
       SRD2 = 0;
       SRD = 0;
       //Clear both status registers of loop   
       SRL = 0;
       SRL2 = 0;
       //Reset message has not been transfered to RF module yet       
       if (BitTest(SRC2,RSTFLG) == 0) BitSet(SRD2,RSTFLG);
    } 
                                                 
    // PRG request has been received from RM
    if (BitTest(SRC2,PRGFLG)==1) BitRST(SRD2,PRGFLG);         
    
    // CMD request has been received from RM
    if (BitTest(SRC2,RMFLG)==1) BitRST(SRD2,RMFLG);   
}

//Setting of SCR(status command register)
//Setting is done after successful SPI transfer between CPU and RM
//SCR and SCR2 bits represent succesfully sent commands to RM
// SCR >>> IFLG - initialisation flag
// SCR >>> BFLG - low battery flag
// SCR >>> PFLG - prealarm flag
// SCR >>> FFLG - alarm flag

// SCR2 >>> TFLG - test flag
// SCR2 >>> DFLG - diagnostic flag
// SCR2 >>> PRGFLG - programing flag
// SCR2 >>> RMFLG - RM command flag
// SCR2 >>> SETLFLG - sensor settiling flag
// SCR2 >>> RSTFLG - Reset flag

//Not implemented yet
// SCR >>> HFLG - Hush flag - Not implemented yet
// SCR >>> CFLG - comm. flag - Not implemented yet
// SCR >>> NFLG - note flag - Not implemented yet 
void SRCsetting(){
    // RESET solution    
    if (BitTest(SRD2,RSTFLG)==1){
        // Clear both command status registers
        SRC = 0;
        SRC2 = 0;                             
        // Set RSTFLG to indicate that RST has been successful
        BitSet(SRC2,RSTFLG);
    }
    else BitRST(SRC2,RSTFLG);
    
    // Indicate that ALARM SPI transfer has been successful
    if (BitTest(SRD,FFLG)==1) BitSet(SRC,FFLG);
    else BitRST(SRC,FFLG);          
    // Indicate that FAULT SPI transfer has been successful
    if (BitTest(SRD,EFLG)==1) BitSet(SRC,EFLG);
    else BitRST(SRC,EFLG);                  
    // Indicate that LOW BATTERY SPI transfer has been successful
    if (BitTest(SRD,BFLG)==1) BitSet(SRC,BFLG);
    else BitRST(SRC,BFLG);                                 
    // Indicate that TEST SPI transfer has been successful
    if (BitTest(SRD2,TFLG)==1) BitSet(SRC2,TFLG);
    else BitRST(SRC2,TFLG);                               
    // Indicate that HUSH SPI transfer has been successful
    if (BitTest(SRD,HFLG)==1) BitSet(SRC,HFLG);
    else BitRST(SRC,HFLG);                               
    // Indicate that DIAGNOSTIC SPI transfer has been successful
    if (BitTest(SRD2,DFLG)==1) BitSet(SRC2,DFLG);
    else BitRST(SRC2,DFLG); 
    
    // Indicate that PROGRAMING Mode Request SPI transfer has been successful
    if (BitTest(SRD2,PRGFLG)==1) BitSet(SRC2,PRGFLG);
    else BitRST(SRC2,PRGFLG);
    // Indicate that RF COMMAND Request SPI transfer has been successful
    if (BitTest(SRD2,RMFLG)==1) BitSet(SRC2,RMFLG);
    else BitRST(SRC2,RMFLG);
}

//SRL Status Register of the loop
//Setting according to received SPI command in SPI_message2CPU[SPI_COMMAND]

void SRLsetting(){
    //When RESET has been done
    if (BitTest(SRC2,RSTFLG)==1){
        SRL = 0;
        SRL2 = 0;
    }
            
    switch(SPI_message2CPU[SPI_COMMAND]){
    case SPI2CPU_TEST:
         BitSet(SRL2,TFLG);
    break;
    case SPI2CPU_BATTERY:
         BitSet(SRL,BFLG);
    break;
    case SPI2CPU_RESET:
         //Remote RESET has been done    
         //Clear all loop indication
         SRL = 0;
         SRL2 = 0;
         //Clear all detector indication
         SRD = 0;
         SRD2 = 0;
         //Clear all command indication
         SRC = 0;
         SRC2 = 0;
    break;    
    };    
}

void Indication(){
unsigned char i;
   ALARM_LED = 0;
   FAULT_LED = 0;
    
   //Initialisation - RED LED and YELLOW LED short double flash
   if (BitTest(SRD,IFLG)==1){
        ALARM_LED = 1;
        FAULT_LED = 1;                 
        delay_ms(25);      
        FAULT_LED = 0;
        ALARM_LED = 0;
        delay_ms(25);      
        ALARM_LED = 1;
        FAULT_LED = 1;                 
        delay_ms(25);      
        FAULT_LED = 0;
        ALARM_LED = 0; 
   }
   
   //Reset - FAUL LED flutters
   if ((BitTest(SRD2,RSTFLG)==1)||(BitTest(SRL2,RSTFLG)==1)){
        for(i=0; i<10; ++i){ 
            FAULT_LED ^= 1;
            delay_ms(20);
            FAULT_LED ^= 1;
            delay_ms(20);      
        }
        FAULT_LED = 1; //LED is on until RST is done 
   }
   
   //Prealarm - RED LED short flash
   if (BitTest(SRD,PFLG)==1){
        ALARM_LED = 1;
        delay_ms(100);
        ALARM_LED = 0;
   }    
   //Full Alarm - RED LED lights
   if (BitTest(SRD,FFLG)==1) ALARM_LED = 1;
   else if (BitTest(SRL,FFLG)==1){
        ALARM_LED = 1; 
        delay_ms(50);
        ALARM_LED = 0; 
        delay_ms(50);
        ALARM_LED = 1; 
        delay_ms(50);
        ALARM_LED = 0; 
   }
   
   if (BitTest(SRD,FFLG)==1) OUTDARL = 1;
   //Diagnostic - RED LED flutters 
   if (BitTest(SRD2,DFLG)==1){
      if (BitTest(SRC2,DFLG)==0){
        for(i=0; i<10; ++i){ 
            ALARM_LED ^= 1;
            delay_ms(20);
            ALARM_LED ^= 1;
            delay_ms(20);      
        }
      }
      for(i=0; i<10; ++i){
        ALARM_LED ^= 1; 
        delay_ms(20);
        ALARM_LED ^= 1;  
        delay_ms(20);    
      }
   }
   
   //RF module Programing mode - Fault LED lights + RED LED short flash            
   if (BitTest(SRD2,PRGFLG)==1){
      FAULT_LED = 1;                 
      ALARM_LED = 1;
      delay_ms(25);      
      ALARM_LED = 0;
   }  
   
   //Test - RED + YELLOW lights             
   if (BitTest(SRD2,TFLG)==1){
      ALARM_LED = 1;
      FAULT_LED = 1;
   }
   else if (BitTest(SRL2,TFLG)==1){
      ALARM_LED = 1;
      FAULT_LED = 1;      
   }
   
   //Communication I2C Fault - YELLOW LED lights + 2x RED LED short flash  
   if (I2Ctransfer > 0x00){
      FAULT_LED ^= 1;                 
      ALARM_LED = 1;
      delay_ms(25);      
      ALARM_LED = 0;
      delay_ms(25);
      ALARM_LED = 1;
      delay_ms(25);      
      ALARM_LED = 0;
   }                                                                                 
   
   //Communication SPI Fault - YELLOW LED lights + 3x RED LED short flash   
   if (SPItries > MAXSPITRIESCNT){
      FAULT_LED ^= 1;                 
      ALARM_LED = 1;
      delay_ms(25);      
      ALARM_LED = 0;
      delay_ms(25);
      ALARM_LED = 1;
      delay_ms(25);      
      ALARM_LED = 0; 
      delay_ms(25);
      ALARM_LED = 1;
      delay_ms(25);      
      ALARM_LED = 0;
   }
   
   //LOW battery - YELLOW LED double fast blink
   if (BitTest(SRD,BFLG)==1){
      FAULT_LED ^= 1;                 
      delay_ms(25);      
      FAULT_LED ^= 1; 
      delay_ms(25); 
      FAULT_LED ^= 1;                 
      delay_ms(25);      
      FAULT_LED ^= 1;
   }
   
   //Hush - YELLOW LED blink
   if (BitTest(SRD,HFLG)==1){
      FAULT_LED ^= 1;                 
      delay_ms(100);      
      FAULT_LED ^= 1; 
   }
   
   if (BitTest(SRD2,SETLFLG)==1){
      FAULT_LED ^= 1;                 
      delay_ms(10);      
      FAULT_LED ^= 1; 
   }
   
   if (BitTest(SRD2,RMFLG)==1){
       switch(command2RF){
          case SPI2RF_REPEATER_ON: 
            ALARM_LED = 1;
            FAULT_LED = 1;
            delay_ms(200);
            FAULT_LED = 0;
            delay_ms(200);
            FAULT_LED = 1;
            delay_ms(200);
            FAULT_LED = 0;
          break;
          case SPI2RF_REPEATER_OFF:
            ALARM_LED = 0;
            FAULT_LED = 1;
            delay_ms(200);
            FAULT_LED = 0;
            delay_ms(200);
            FAULT_LED = 1;
            delay_ms(200);
            FAULT_LED = 0;
          break;
          case SPI2RF_DELETE:
            ALARM_LED = 1;
            FAULT_LED = 0;
            delay_ms(200);
            ALARM_LED = 0;
            FAULT_LED = 1;
            delay_ms(200);
            ALARM_LED = 1;
            FAULT_LED = 0;
            delay_ms(200);
            ALARM_LED = 0;
            FAULT_LED = 1; 
            delay_ms(200);
            ALARM_LED = 0;
            FAULT_LED = 0;
          break;
          default:
          break;     
       }
   
   }            
}

void CreateRFmessage(){
    SPI_message2RF[SPI_ID]        = tempObj;
    SPI_message2RF[SPI_SUBLOOP]   = tempA;
    SPI_message2RF[SPI_ZONE]      = SRD;
    SPI_message2RF[SPI_COMMAND]   = command2RF;
    
    if (command2RF == SPI2RF_NOTHING){      
        if ((BitTest(SRD,FFLG)==1)&&(BitTest(SRC,FFLG)==0)) SPI_message2RF[SPI_COMMAND] = SPI2RF_ALARM;
        
        if ((BitTest(SRD,EFLG)==1)&&(BitTest(SRC,EFLG)==0)) SPI_message2RF[SPI_COMMAND] = SPI2RF_FAULT;
        
        if ((BitTest(SRD,BFLG)==1)&&(BitTest(SRC,BFLG)==0)) SPI_message2RF[SPI_COMMAND] = SPI2RF_BATTERY;
        
        if ((BitTest(SRD2,TFLG)==1)&&(BitTest(SRC2,TFLG)==0)) SPI_message2RF[SPI_COMMAND] = SPI2RF_TEST;
        
        if ((BitTest(SRD,HFLG)==1)&&(BitTest(SRC,HFLG)==0)) SPI_message2RF[SPI_COMMAND] = SPI2RF_HUSH;   
        
        if (((BitTest(SRD,BFLG)==0)&&(BitTest(SRD,EFLG)==0))&&((BitTest(SRC,BFLG)==1)||(BitTest(SRC,EFLG)==1)))
            SPI_message2RF[SPI_COMMAND] = SPI2RF_CLEAR; 
        
        if (BitTest(SRD2,RSTFLG)==1) SPI_message2RF[SPI_COMMAND] = SPI2RF_RESET;
        
        if (BitTest(SRD2,DFLG)==1) SPI_message2RF[SPI_COMMAND] = SPI2RF_DIAG;
        
        if (BitTest(SRD2,PRGFLG)==1) SPI_message2RF[SPI_COMMAND] = SPI2RF_PROG;
    }    
}

unsigned char ReadBatteryVoltage(unsigned char input){
unsigned char tmp = 0;
unsigned char tmp2 = 0;
//TODO - test LOW battery detection + 9V battery detection
  if (buttonPressed > 0) return(input);    
  if (batTimeout>0){
      --batTimeout;
      return(input);
  }    
  ALARM_LED = 0;
  FAULT_LED = 0;
  batTimeout = BATTESTTIME;
  ADC_Init();  
  if (BitTest(SRD2,BAT9V) == 1){  
      //Yellow LED loads the battery for 10ms 
      FAULT_LED = 1;
      delay_ms(200);                           
      //Read the pin
      tmp = read_adc(POWERIN);
      FAULT_LED = 0;            
  }
  else{
      //Red LED loads the battery for 10ms   
      ALARM_LED = 1;
      delay_ms(200);
      //Read the pin  
      tmp = read_adc(POWERIN);
      ALARM_LED = 0;     
  }
  ADCSRA = 0x00;
  return(tmp);
}

void ADC_Init(void){
    // ADC initialization
    // ADC Clock frequency: 125,000 kHz
    // ADC Voltage Reference: 1.1V, AREF discon.
    // ADC Bipolar Input Mode: Off
    // ADC Auto Trigger Source: ADC Stopped
    // Only the 8 most significant bits of
    // the AD conversion result are used
    // Digital input buffers on ADC0: Off, ADC1: On, ADC2: Off, ADC3: On
    // ADC4: On, ADC5: On, ADC6: On, ADC7: On
    DIDR0=(0<<ADC7D) | (0<<ADC6D) | (0<<ADC5D) | (0<<ADC4D) | (0<<ADC3D) | (1<<ADC2D) | (0<<ADC1D) | (1<<ADC0D);
    ADMUX=0;
    ADCSRA=(1<<ADEN) | (0<<ADSC) | (0<<ADATE) | (0<<ADIF) | (0<<ADIE) | (1<<ADPS2) | (0<<ADPS1) | (1<<ADPS0);
    ADCSRB=(0<<BIN) | (1<<ADLAR) | (0<<ADTS2) | (0<<ADTS1) | (0<<ADTS0);
}

unsigned char ReadKTY(){
unsigned char i;
unsigned int tmp,tmp2;
  ADC_Init();  
  tmp = read_adcVBAT(KTYIN); 
  ADCSRA = 0x00; 
  if ((BitTest(SRD2,BAT9V) == 0)&&(batV<VAL3V3BAT)){  
    // a = x / 3.3V * 512        b = x / (3.3V - delta) * 512
    // a = (3.3V - delta) * 512 * b / (3.3V * 512)
    // 3.3V = val3V3 / 256 * 1.1V / 22000 * 90000
    // delta = valDelta / 256 * 1.1V / 22000 * 90000
    // a = b - delta/3.3V * b = b - valDelta/val3V3 * b
    // tmp2 = delta/val3V3 
    tmp2 = (VAL3V3BAT-batV);
    tmp2 = tmp2*tmp;
    tmp2 = tmp2/VAL3V3BAT;
    tmp = tmp - tmp2; 
  }                
  
  analog[0] = tmp>>8;
                       
  analog[1] = tmp&0x00FF;
  //Value is too small                                                    
  if (tmp < lookupKTY[0]) return(0xFF);
  
  //Check the lookup table, index of the table coreesponds with temperature
  for(i = 0; i<231;++i){                                                   
    if (lookupKTY[i]>=tmp) break;
  } 
  
  //Value was not found in the table                                                    
  if (i == 231) return(0xFF);
  
  //Value was found in the table                                                   
  if (tmp < 0xFF00){                                     
      tmp = i;
      //Convert the index to �C
      //0 = -55�C, 230 = 175�C
      if (tmp < 56){   
        //Negative temperature 
        tmp = 55-tmp;
        //Set the most significant bit - calculate   
        tmp = ~tmp;        
      }
      else{
        tmp = tmp - 55;
      }                 
  }                                  
  
  //Write the result into temperature buffer
  temperature[1] = (tmp>>8)&0x00FF;
  temperature[0] = tmp&0x00FF;
  return(0);
}  

/*unsigned char ReadKTY(){
unsigned int tmp,tmp2;
unsigned long int tmp3,tmp4;
  ADC_Init();  
  tmp = read_adcVBAT(KTYIN);
  ADCSRA = 0x00;
  analog[0] = tmp;
  //tmp = temperature[0];
  temperature[0] = 0;
  
  //Vbat = 3V3
  //R1 = 3300 Ohm
  //R25 = 1000 Ohm                                        
  //beta = 1.707*10-5    /  beta = 1,731*10-5(used in following equation)  - what is the right
  //alfa = 7.653*10-3                          
  //IT = 1,2mA                                  
  //VT = VTd/256 * 3.3 -> VTd = AD converter result  
  //RS = R25*(1+alfa*Tdiff+beta*Tdiff2) = VT/IT
  //RS = 1000*(1+ 7.653*10-3 *Tdiff+ 1.707*10-5 *Tdiff2)
  //RS = 1000 + 7.653*Tdiff+ 1.707*10-2 *Tdiff2)
  //RS = 1000 + 7.653*Tdiff+ 1.707*10-2 *Tdiff2)
  //0 = 1000-VT/IT + 7.653*Tdiff+ 1.707*10-2 *Tdiff2
  //x12 = (-b+/-sqrt(b2-4ac))/2a 
  //58,57-0,068*(1000-VT/IT)= (58,57 - 68 + 56*VT) * 
  
  //Tdiff12 = 220 +/- {sqrt[-9136 + 48142*VT]}
  //Tdiff12 = 220 +/- {sqrt[-9136 + 620*VTd]}
  //0�C   Tdiff12 = -25 -> Tdiff12 = 220 - {sqrt[-9136 + 620*VTd]} 
  //      VT = 0.984, VTd = 78                     
  //25�C  Tdiff12 = 0 -> Tdiff12 = 220 - {sqrt[-9136 + 620*VTd]}
  //      VT = 0.767V, VTd = 92
  //90�C Tdiff12 = +65 -> Tdiff12 = 220 - {sqrt[-9136 + 620*VTd]}
  //      VT = 1,883V, VTd = 111
  //tmp = 92; 
  tmp2 =620; 
  tmp3 = tmp2*tmp;
  tmp2 = 9136;
  tmp4 = tmp3-tmp2;
  tmp2 = sqrt(tmp4);
  temperature[0] = tmp2;
  if (tmp2 < 220) temperature[0] = (220 - temperature[0]) - 25;
  else temperature[0] = (temperature[0] - 220) + 25;                   
}
*/
// Read the 8 most significant bits
// of the AD conversion result
unsigned char read_adc(unsigned char adc_input)
{
    ADMUX=(adc_input & 0x3f)|ADC_VREF_TYPE;
    // Delay needed for the stabilization of the ADC input voltage
    delay_us(10);
    // Start the AD conversion
    ADCSRA|=(1<<ADSC);
    // Wait for the AD conversion to complete
    while ((ADCSRA & (1<<ADIF))==0);
    ADCSRA|=(1<<ADIF);
    return ADCH;
}

unsigned int read_adcVBAT(unsigned char adc_input)
{
unsigned int tmp,tmp2;
    ADMUX=(adc_input & 0x3f) | ADC_VREF_TYPE3V3;
    // Delay needed for the stabilization of the ADC input voltage
    delay_us(10);
    // Start the AD conversion
    ADCSRA|=(1<<ADSC);
    // Wait for the AD conversion to complete
    while ((ADCSRA & (1<<ADIF))==0);
    ADCSRA|=(1<<ADIF);
    tmp = ADCL;   
    tmp2 = ADCH;
    tmp2 = (tmp2<<1);
    tmp = (tmp>>7);
    tmp2 = tmp2 | tmp;
    return tmp2;
}

void timerHandler(){
    if (BitTest(SRD,HFLG)){
        ++timerHush;
        timerTemp = 0;                 
        timerGrad = 0;
    }
    else timerHush = 0;      
    if (BitTest(SRD2,TFLG)) ++timerTest;
    else timerTest = 0;
    
}
    
unsigned char checkTempLimits(){
unsigned char tmp;

    if ((tempA&0x8000)>0) return(0xFF);
    if (tRoom == 0xFF) return(0xFF);
    if (BitTest(SRD,HFLG)) return(0xFF);
    
    //Test Hush condition 
    if (((tempObj > BODYTEMPMIN)&&(tempObj < BODYTEMPMAX))&&(!BitTest(SRD,IFLG))){      //Body temperature - hand covvers the sensor
        if (tempGrad[sampleCNT-2]>tempGrad[sampleCNT-1]){
            tmp = tempGrad[sampleCNT-2]-tempGrad[sampleCNT-1];
        }
        else{
            tmp = tempGrad[sampleCNT-1]-tempGrad[sampleCNT-2];
        } 
        if (tmp > 3){        //The temperature difference is 4 grads
            BitSet(SRD,HFLG); //Set the Hush
            return(0xFF);    
        }
    }    
    
    // Increment timerTemp when sensor temperature is high 
    
    //if (tempA > tCondLimit+tRoom){
    //Measurement of object temperature increase minimal Temperature + increase 
    if (tempObj > (tCondLimit+gTempMIN)){ 
        if (timerTemp<255) ++timerTemp;
    }
    else timerTemp = 0;   
    
          
    // Increment timerGrad when gradient of the temperature is too high 
    if (tempGradAct > gCondLimit){
        if (timerGrad<255) ++timerGrad;   
    }
    else timerGrad = 0; 
    
    // Check Ambient temperature and when the temp is above 70�C 
    if (tempA > SENSORAMBIENTTEMP){
        if (timerGrad<255) ++timerGrad; 
        if (timerTemp<255) ++timerTemp;  
    }         
} 
         
unsigned char storeTempAndGradCalc(unsigned int temp){  
    //Cancel if the temperature is under -70�C
    if ((temp&0x8000)&&(temp<0xFFBA)) return(0xFF);
    //Cancel if the temperature is above 380�C
    if (temp > 0x017C) return(0xFF);
    
    //Store object temperature to tmeperature buffer 
    tempGrad[tempGradindex] = temp;                         
    ++tempGradindex;
    //if there is already required sample count calculate gradient
    if(tempGradindex > sampleCNT){                         
        --tempGradindex;     
        //Gradient would be here calculated as (tempGrad[tempGradindex]-tempGrad[0])/sampleCNT
        //Grad limit setting multiplied by 100
        //Calculate onlz positive gradient, otherwise get 0 
        if (tempGrad[tempGradindex]>tempGrad[0]){
            tempGradAct = (tempGrad[tempGradindex]-tempGrad[0])*100;
            tempGradAct = tempGradAct/sampleCNT;
        }
        else tempGradAct = 0; 
        moveTempGrad(sampleCNT);
    }
    return(0);
}

unsigned char AverageRoomTempCalc(unsigned int temp){
unsigned char i;
unsigned int x;
  
    // Cancel if the temperature is under -70�C
    if ((temp&0x8000)&&(temp<0xFFBA)) return(0xFF);
    // Cancel if the temperature is above 380�C
    if (temp > 0x017C) return(0xFF);
                             
    //
    ++calcAverageRoomTindex;    
    //Temperature of the room is initializing
    if (tRoom == 0xFF) calcAverageRoomTindex = sampleCNT;
    
    // if there is already required sample count store temp for average calc 
    if(calcAverageRoomTindex >= sampleCNT){ 
    //if(calcAverageRoomTindex >= 2){
        // Reset timer
        calcAverageRoomTindex = 0;
        // Store room temperature to temperature buffer
        tempRoom[averageRoomTindex] = temp;                                              
        // Store room temperature buffer
        ++averageRoomTindex;
        //if there is already required sample count calculate average
        if(averageRoomTindex >= 16){ 
            --averageRoomTindex;
            // When room temperature is initialized
            if (tRoom == 0xFF) x  = 25;
            else x = tRoom;  
            for(i = 0;i<sampleCNT-1;++i){
                x = x+tempRoom[i];
            }            
            if ((tRoom%sampleCNT)>5) x = x/sampleCNT+1; 
            else x = x/sampleCNT;
            moveTempRoom(sampleCNT);
            //May be could be filtered max and min value
            tRoom = x;
        }
    }
    return(0);
}

//Shifts tempGrad[i] <= tempGrad[i+1]. Count = size of the tempGrad. 
//tempGrad[0] = tempGrad[1]
//...
//tempGrad[count-2] = tempGrad[count-1] 
void moveTempGrad(unsigned char count){
unsigned char i;
    for(i = 0; i < count; ++i){
        tempGrad[i] = tempGrad[i+1];  
    } 
}

//Shifts tempRoom[i] <= tempRoom[i+1]. Count = size of the tempRoom. 
//tempRoom[0] = tempRoom[1]
//...
//tempRoom[count-2] = tempRoom[count-1] 
void moveTempRoom(unsigned char count){
unsigned char i;
    for(i = 0; i < count; ++i){
        tempRoom[i] = tempRoom[i+1];  
    } 
}

unsigned char EEPROM_read(unsigned int addr)
{
/* Wait for completion of previous write */
while(EECR & (1<<EEPE));
/* Set up address register */
EEARH = 0;
EEARL = addr;
/* Start eeprom read by writing EERE */
EECR |= (1<<EERE);
/* Return data from data register */
return EEDR;
}

void EEPROM_write(unsigned int addr, unsigned char data)
{
/* Wait for completion of previous write */
while(EECR & (1<<EEPE));
/* Set Programming mode */
EECR = (0<<EEPM1)|(0<<EEPM0);
/* Set up address and data registers */
EEARH = 0;
EEARL = addr;
EEDR = data;
/* Write logical one to EEMPE */
EECR |= (1<<EEMPE);
/* Start eeprom write by setting EEPE */
EECR |= (1<<EEPE);
}

unsigned char DiagnosticMessage(unsigned char SPIDLEN){
unsigned char data;
unsigned char CRCM,CRCS,i;
    SET_MOSIOUT;
    MOSI = 0; 
    SET_MISOIN;
    MISO = 1;
    SET_SCKOUT;
    SCK = 0;          
    SPI_Byte2RF(SPI_CHECK);         //First sends SPI_CHECK
    SPI_Byte2RF(SPI_CHECK);         //First sends SPI_CHECK
    data = USIDR;
    if  (data == 0x80){
        SPI_Byte2RF(SPI_CMD);
        SPI_Byte2RF(PTYPE+SPIDLEN+8);
        CRCM = SPI_CMD^(PTYPE+SPIDLEN+8);     
        //Start sequence
        data = 0xFF;
        for (i = 0;i<4;++i){
            SPI_Byte2RF(data);
            CRCM = CRCM^data;
        }
        //Type of the message
        if (SPIDLEN > 10) data = 0;
        else  data = 1;
        SPI_Byte2RF(data);
        CRCM = CRCM^data;
        //Length of the message
        data = SPIDLEN-6;
        SPI_Byte2RF(SPIDLEN);
        CRCM = CRCM^data;
        //Program
        data = EEPROM_read(EEPROG);
        SPI_Byte2RF(data); 
        CRCM = CRCM^data;
        //Version
        data = EEPROM_read(EEVER);
        SPI_Byte2RF(data);
        CRCM = CRCM^data;
         //Subversion
        data = EEPROM_read(EESUB);
        SPI_Byte2RF(data);
        CRCM = CRCM^data;
        // Message Type 0
        if (SPIDLEN > 10){           
            //Start serial Number
            for (i = 0;i<3;++i){
                data = EEPROM_read(EESERIALN+i);
                SPI_Byte2RF(data);
                CRCM = CRCM^data;  
            }
            // 
            data = EEPROM_read(EEGRADCOND);
            SPI_Byte2RF(data);   
            CRCM = CRCM^data;
            // 
            for (i = 0;i<6;++i){
                data = EEPROM_read(EECHECKSUM+i);
                SPI_Byte2RF(data);
                CRCM = CRCM^data;  
            }
            //
            data = EEPROM_read(EETEMPCOND);
            SPI_Byte2RF(data);   
            CRCM = CRCM^data;
            //History log
            for (i = 0;i<33;++i){
                data = EEPROM_read(EEHISTORYSTART+i);
                SPI_Byte2RF(data);
                CRCM = CRCM^data;  
            }
            //
            data = EEPROM_read(EESAMPLESCNT);
            SPI_Byte2RF(data);   
            CRCM = CRCM^data;
            //
            for (i = 0;i<6;++i){
                data = 0;
                SPI_Byte2RF(data);
                CRCM = CRCM^data;  
            }
        }
        else{
            //Object Temperature HB 
            data = tempObj>>8;
            SPI_Byte2RF(data);   
            CRCM = CRCM^data;
            //Object Temperature LB 
            data = tempObj;
            SPI_Byte2RF(data);   
            CRCM = CRCM^data;
            //Sensor Temperature 
            data = tempA;
            SPI_Byte2RF(data);   
            CRCM = CRCM^data;
            //Temperature Gradient 
            data = timerGrad;
            SPI_Byte2RF(data);   
            CRCM = CRCM^data;
            //Temperature  
            //data = timerTemp;
            //SPI_Byte2RF(data);   
            //CRCM = CRCM^data;
            //Temperature Gradient 
            //data = batV;
            data = tempGrad[sampleCNT-2];
            SPI_Byte2RF(data);   
            CRCM = CRCM^data;
            //Temperature Gradient 
            //data = tRoom;
            data = tempGrad[sampleCNT-1];
            SPI_Byte2RF(data);   
            CRCM = CRCM^data;
            //SRD 
            data = SRD;
            SPI_Byte2RF(data);   
            CRCM = CRCM^data;
        }          
        
        for (i = 0;i<2;++i){
            data = 0xFE;
            SPI_Byte2RF(data);
            CRCM = CRCM^data;  
        }
        CRCM = CRCM^0x5F; 
        SPI_Byte2RF(CRCM); 
        data = USIDR;          
        SPI_Byte2RF(SPI_CHECK);         
        if  (USIDR == 0x3F){
            data = USIDR;
        }                  
        else{
            data = 0xFF;
        }           
    }
    else{ 
        data = 0xFF;
    } 
    USICR=0x00;            
    SET_SCKIN;
    SET_MOSIIN;
    return(data);  
}

void buttonFunction(){
unsigned char i;  
    //Short push of the button 
    if (buttonPressed == BUTTONSTEP0){  
        //Test if diagnostic state is on -> Switch the diagnostic off
        if(BitTest(SRC2,DFLG)) BitRST(SRD2,DFLG);
        //Test if some event is activated -> run reset
        else if ((SRC2>0)||(SRC>0)||(SRL>0)||(SRL2>0)){
            BitSet(SRD2,RSTFLG);  
        }        
        //Test if hush is off -> Turn on Hush state
        else if(!BitTest(SRD,HFLG)) BitSet(SRD,HFLG);                                              
        //else if(!BitTest(SRD2,TFLG)) BitSet(SRD2,TFLG);
        
        //Test Of Gradient condition
        /*ALARM_LED = 1;
        delay_ms(200);
        ALARM_LED = 0;
        for(i = 0; i < 6; ++i){
            tempGrad[i] = tempGrad[i]-3;  
        } */
    }  
    //Long button press
    // Off the hush and run test state
    if (buttonPressed == BUTTONSTEP1){ //12
        BitRST(SRD,HFLG);        
        if(!BitTest(SRD2,TFLG)) BitSet(SRD2,TFLG);        
    } 
    //Long Button press               
    // Off the test and run diagnostic
    if (buttonPressed == BUTTONSTEP2){ //16
        BitRST(SRD2,TFLG);        
        if(!BitTest(SRD2,DFLG)) BitSet(SRD2,DFLG);        
    } 
    //RFButton press()
    //Off the diagnostic and Set Repeater ON                      
    if (buttonPressed == BUTTONSTEP3){ //20                   
        command2RF = SPI2RF_REPEATER_ON;
        BitRST(SRD2,DFLG);
        if(!BitTest(SRD2,RMFLG)) BitSet(SRD2,RMFLG);        
    }
    //Set Repeater OFF
    if (buttonPressed == BUTTONSTEP4){                    
        command2RF = SPI2RF_REPEATER_OFF;
       // BitRST(SRD2,RMFLG);   
       // if(!BitTest(SRD2,PRGFLG)) BitSet(SRD2,PRGFLG);        
    }                 
    //Delete RF module 
    if (buttonPressed == BUTTONSTEP5){                    
        command2RF = SPI2RF_DELETE;
    }
    //Set programing mode on
    if (buttonPressed == BUTTONSTEP6){       
        command2RF = SPI2RF_NOTHING;             
        BitRST(SRD2,RMFLG);   
        if(!BitTest(SRD2,PRGFLG)) BitSet(SRD2,PRGFLG);        
    }                                    
    // Interrupt initialisation
    if (PUSHBUTTON){
       buttonPressed = 0;
       GIMSK=(1<<INT0) | (0<<PCIE1) | (0<<PCIE0);
       GIFR=(1<<INTF0) | (0<<PCIF1) | (0<<PCIF0);
       #asm("sei");
    }
    else if ((buttonPressed<BUTTONSTEPMAX)&&(BitTest(SRD2,RSTFLG)==0)) {
       ++buttonPressed;
    }      
}