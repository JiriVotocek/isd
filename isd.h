
#define BitTest(a,b)            ((a>>b)&(0x0001))
#define BitSet(a,b)             a = (a|(1<<b))
#define BitRST(a,b)             a = (a&(0xFFFF^(1<<b)))
#define BitNeg(a,b)             a = (a^(0x0000|(1<<b)))


//Status register of detector(SRD) bit meaning
#define    FFLG     7       // ALARM
#define    HFLG     6       // ALARM MEMORY
#define    EFLG     5       // ERROR
#define    IFLG     4    	// INITIALISATION
#define	   CFLG	    3		// COMMUNICATION - DIAG. MODE
#define	   NFLG	    2		// NOTE  - po�adavek z�znamu ud�losti do EE
#define	   BFLG     1		// BATT - indik�tor n�zk� batterie 										*
#define    PFLG     0		// PreAlarm 

//Status register of detector(SRD2) bit meaning
#define    TFLG     7        // TEST
#define    DFLG     6        // Diagnostic
#define    PRGFLG   5        // RF program uppload
#define    RMFLG    4        // Controll of RF module
#define    SETLFLG  3        // Settling of sensor
#define    RSTFLG   2        // Reset of detector

//Status register2 of detector(SRD2) bit meaning              
#define    KTYSENS  1		// KTY sensor connected
#define    BAT9V    0		// 9V battery connected 

// LED indication
#define ALARM_LED   PORTB.0 // RED LED
#define FAULT_LED   PORTB.1 // YELLOW LED 
 
//Inputs 
#define RESETPIN    PINB.3  // RESET PIN position
#define PUSHBUTTON  PINB.2  // PUSHBUTTON control of the detector

#define OUTDARL     PORTA.7 // Output transistor control

// Voltage Reference: 1.1V, AREF discon.
#define ADC_VREF_TYPE ((1<<REFS1) | (0<<REFS0))
// Voltage Reference: 3,3V, AREF discon.
#define ADC_VREF_TYPE3V3 0x00//((0<<REFS1) | (0<<REFS0))
#define KTYIN       2       //ADC position of KTY sensor
#define POWERIN     0       //ADC position of power pin

//SPI Peripheral Setting
#define MOSI        PORTA.5
#define SET_MOSIOUT DDRA.5 = 1
#define SET_MOSIIN  DDRA.5 = 0
#define MISO        PORTA.6
#define SET_MISOOUT DDRA.6 = 1
#define SET_MISOIN  DDRA.6 = 0
#define SCK         PORTA.4
#define SET_SCKOUT  DDRA.4 = 1
#define SET_SCKIN   DDRA.4 = 0
#define SS          PORTA.3
//Set USI interface to SPI
//USIWM0 = 1 - SPI
//USICLK = 1 & USICLK = 1  -  Software clock strobe
//USITC = 1(4-bit counter ON) - Master
#define SPIINTERFACE_ON USICR = (1<<USIWM0)|(1<<USICS1)|(1<<USICLK)|(1<<USITC)

//SENSOR control
#define SENSORKTYPOWERPINOUT    DDRA.2 = 1
#define SENSORKTYPOWERPININ     DDRA.2 = 0
#define SENSORKTYPOWERPLUP      PORTA.2 = 1
#define SENSORKTYPOWERHZ        PORTA.2 = 0
//#define SENSORKTYPOWERON        PORTA.2 = 0
//#define SENSORKTYPOWEROFF       PORTA.2 = 1

#define ALLSENSORMAINPOWERON    PORTA.1 = 0
#define ALLSENSORMAINPOWEROFF   PORTA.1 = 1
//
#define I2C_ADR	                0x5A          //I2C Melexis address
#define COMMAND_Tobj            0b00000111 //Command for PYRO sensor to get object temperature
#define COMMAND_Ta              0b00000110 //Command for PYRO sensor to get device temperature
#define COMMAND_SLEEP           0b11111111 //Command for PYRO sensor to set device sleep
#define COMMAND_FLAGS           0b11110000 //Command for PYRO sensor to get status flags
#define COMMAND_EEPROM          0b00010000 //Command for PYRO sensor to get status flags

//I2C Peripheral Setting
#define I2CINTERFACE_ON USICR = (1<<USIWM1)|(1<<USICLK)

#define MLXSDA         PORTA.6
#define MLXSDAin       PINA.6
#define SET_MLXSDAOUT  DDRA.6 = 1
#define SET_MLXSDAIN   DDRA.6 = 0
#define MLXSCL         PORTA.4
#define MLXSCLin       PINA.4
#define SET_MLXSCLOUT  DDRA.4 = 1
#define SET_MLXSCLIN   DDRA.4 = 0

//RF module SPI handling commands
#define SPI_CHECK           0x00 
#define SPI_CMD             0xF0
#define PTYPE               0x80
#define SPI_ID              0
#define SPI_SUBLOOP         1
#define SPI_ZONE            2
#define SPI_COMMAND         3

#define SPI2RF_NOTHING      'N'
#define SPI2RF_ALARM        'F'
#define SPI2RF_FAULT        'E'
#define SPI2RF_BATTERY      'B'
#define SPI2RF_RESET        'R'
#define SPI2RF_PROG         'P'
#define SPI2RF_TEST         'T'
#define SPI2RF_HUSH         'H'
#define SPI2RF_CLEAR        'C'
#define SPI2RF_DIAG         '3'
#define SPI2RF_REPEATER_ON  '-'
#define SPI2RF_REPEATER_OFF '+'
#define SPI2RF_DELETE       'D'

#define SPI2CPU_NOTHING     'n'
#define SPI2CPU_ALARM       'f'
#define SPI2CPU_FAULT       'e'
#define SPI2CPU_BATTERY     'b'
#define SPI2CPU_RESET       'r'
#define SPI2CPU_PROG        'p'
#define SPI2CPU_TEST        't'
#define SPI2CPU_HUSH        'h'
#define SPI2CPU_CLEAR       'c'
#define SPI2CPU_DIAG        '3'
#define SPI2CPU_DELETE      'd'


#define MAXSPITRIESCNT      4

//Condition limits for event evaluation
#define MAXSAMPLECNT        30          //Max measured object temperature value 
#define MAXTEMPCOND         30          //Max measured object temperature value 
#define VAL3V3BAT           187         //Battery value 3.3V (dividing by 4.0909 internal reference 1.1V)
//#define VAL3V6BAT           204         //Battery value 3.6V (dividing by 4.0909 internal reference 1.1V) - AD conversion shows 206
#define MINBATCOND          182         //Min battery value 3.2V (dividing by 4.0909 internal reference 1.1V)
                                        //Min battery value 7.6V when 9V battery is used
#define MAXBAT3VCOND        254         //Min battery value 4.5V (dividing by 4 internal reference 1.1V)
#define BATTESTTIME         15
#define MAXTIMEPRECOND      10          //x period
#define MAXTIMECOND         20          //x period
#define KTYASSEMBLED        121         //3V3 reference - 0,786V 
#define MAXKTYASSEMBLED     204         //3V3 reference - 1,314V measured when 1100 Ohm(40�C) 
#define GRADCOUNT           10
#define MAXGRADCOND         10
#define BODYTEMPMIN         30
#define BODYTEMPMAX         37
#define SENSORAMBIENTTEMP   70

//Button push constants - in main program cycles count
#define    BUTTONSTEP0     10       	// Intitial value after push - RST or HUSH
#define    BUTTONSTEP1     12       	// TEST
#define    BUTTONSTEP2     16   		// DIAGNOSTICS
#define    BUTTONSTEP3     20   		// REPEATER ON
#define	   BUTTONSTEP4     24			// REPEATER OFF
#define	   BUTTONSTEP5     28			// DELETE
#define	   BUTTONSTEP6     32			// RF Prog mode
#define	   BUTTONSTEPMAX   36			// Max Button steps

//EEPROM address of condition settings
#define EESAMPLESCNT        0x20
#define EEGRADCOND          0x21
#define EEGRADCONDKTY       0x22
#define EEGRADCNT           0x23
#define EETEMPCOND          0x24
#define EETEMPCONDKTY       0x25
#define EETEMPCNT           0x26
#define EEMINTEMPOBJ        0x27
#define EEMINTEMPOBJKTY     0x28
#define EEFIRETEMPCNT       0x29
#define EEHUSHCNT           0x2A
#define EETESTCNT           0x2B

//EEPROM address of detector FW and SN 
#define EEPROG              0x0D
#define EEVER               0x0E
#define EESUB               0x0F
#define EESERIALN           0x10  
#define EECHECKSUM          0x13
#define EESTARTCNT          0x14
#define EEFAULCNT           0x15
#define EEALARMCNT          0x16
#define EEPREALARMCNT       0x17
//#define EETESTCNT           0x18
#define EEWDTCNT            0x19

#define EEHISTORYSTART      0x30
void CPUinit(void);

//SPI transmiting routine
void SPI_Byte2RF(unsigned char data);
unsigned char SPI_Transfer2RF(unsigned char SPIDLEN);

unsigned char ReadTempSensor();

//I2C transmiting routine
unsigned char I2C_Byte2PyroFlags(unsigned char sAddr);
unsigned char I2C_Byte2Pyro(unsigned char sAddr,unsigned char data);
void I2C_Pyro2WakeUP(void);
unsigned char I2C_Pyro2Sleep(unsigned char sAddr);
void I2CStartCond();
void I2CStopCond();
unsigned char I2CRead(unsigned char ack);
unsigned char I2CWrite(unsigned char data);
unsigned char I2CgetAck();
unsigned char getCRC8(unsigned char *data,unsigned char len);

unsigned char ReadBatteryVoltage(unsigned char input);

void ADC_Init(void);
unsigned char read_adc(unsigned char adc_input);
unsigned int read_adcVBAT(unsigned char adc_input);

//Status detector register setting procedure
void SRDsetting();

//Status communication register setting procedure 
void SRCsetting();

//Status loop register setting procedure 
void SRLsetting();
//Getting of temperature from KTY sensor
unsigned char ReadKTY();

void moveTempGrad(unsigned char count);
void moveTempRoom(unsigned char count);
unsigned char storeTempAndGradCalc(unsigned int temp);
unsigned char checkTempLimits();
unsigned char AverageRoomTempCalc(unsigned int temp);

//EEPROM functions 
unsigned char EEPROM_read(unsigned int addr);
void EEPROM_write(unsigned int addr, unsigned char data);

//LED indication procedure
void Indication();

//Prepare 4bytes message for RF module
void CreateRFmessage();

unsigned char DiagnosticMessage(unsigned char SPIDLEN);

void buttonFunction();

void timerHandler();